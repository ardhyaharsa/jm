$(document).ready(function()
{
  var character = [];
  var religion = null , gender = null;

  var format = 'DD-MM-YYYY';
  $('input[name=birthdate]').daterangepicker({
    singleDatePicker: true,
    startDate: moment($('input[name=birthdate]').val() , format),
    maxDate: moment().subtract(17, 'years'),
    showDropdowns: true,
    locale: {
      format: format
    }
  });

  $(document)
  .on('mouseover', '.filter', function()
  {
    var selected = true;
    var selector = $(this).closest('.filter-group').attr('dt-group');
    switch(selector) {
      case 'character':
        if(character.length == 5 && $(this).attr('dt-order') == 0)
        {
          $(this).addClass('disabled');
          selected = false;
        }
        break;
    }

    if(selected) $(this).addClass('has-background-dark');
  })
  .on('mouseleave', '.filter', function()
  {
    var selected = true;
    var selector = $(this).closest('.filter-group').attr('dt-group');
    switch(selector) {
      case 'character':
        if($(this).attr('dt-order') != 0) selected = false;
        break;

      case 'religion':
      case 'gender':
      if($(this).attr('dt-selected') != 0) selected = false;
      break;
    }

    if(selected) $(this).removeClass('has-background-dark disabled');
  })
  .on('click', '.filter', function()
  {
    var selector = $(this).closest('.filter-group').attr('dt-group');
    switch(selector) {
      case 'character':
        var character_id = $(this).attr('dt-id');
        if($(this).attr('dt-order') > 0)
        {
          var current_order = $(this).attr('dt-order');
          character.splice( character.indexOf(character_id) , 1 );
          $('.filter-group .filter').each(function( index )
          {
            if($(this).closest('.filter-group').attr('dt-group') == selector)
            {
              var filter_order = $(this).attr('dt-order');
              if(filter_order > current_order) $(this).attr('dt-order' , filter_order - 1);
            }
          });

          $(this).attr('dt-order' , 0);
        }
        else if(character.length < 5)
        {
          character.push(character_id);
          $(this).attr('dt-order' , character.length);
        }

        break;

      case 'religion':
      case 'gender':
        var filter_id = 0;
        var selected = $(this).attr('dt-selected');
        if(selected == 1)
        {
          switch(selector) {
            case 'religion':
              religion = null;
              break;
            case 'gender':
              gender = null;
              break;
          }
        }
        else
        {
          $(this).addClass('has-background-dark');
          filter_id = $(this).attr('dt-id');
          switch(selector) {
            case 'religion':
              religion = filter_id;
              break;
            case 'gender':
              gender = filter_id;
              break;
          }
        }

        $('.filter-group .filter').each(function( index )
        {
          if($(this).closest('.filter-group').attr('dt-group') == selector)
          {
            $(this).attr('dt-selected' , 0);
            $(this).removeClass('has-background-dark');
            if($(this).attr('dt-id') == filter_id) $(this).attr('dt-selected' , 1);
          }
        });

        break;
    }

    refresh_filter(false);
  })
  .on('click', '.button-save', function()
  {
    var height = numeral($('input[name=height]').val());
    var weight = numeral($('input[name=weight]').val());

    var data = {
      height : height.value(),
      weight : weight.value(),
      birthdate : $('input[name=birthdate]').val(),
      religion : religion,
      gender : gender,
      character : JSON.stringify(character)
    };

    $.ajax({
      type : 'POST',
      url : $(this).attr('dt-url'),
      data : data,
      beforeSend : function() {
        NProgress.configure({
          showSpinner: false
        }).start();
      },
      success: function (data) {
        NProgress.done();
        $.notify('Penyimpanan Berhasil' , {
          className : 'success',
          position : 'bottom'
        });
      }
    });
  });

  function refresh_filter(update_character)
  {
    if(update_character) character = [];
    $('.filter-group .filter').each(function( index )
    {
      var selector = $(this).closest('.filter-group').attr('dt-group');
      if(selector == 'character')
      {
        var text = $(this).text();
        if(text.substr(-1) == ']')
        {
          var index = text.lastIndexOf(' ');
          $(this).text(text.substring(0, index));
        }
        if($(this).attr('dt-order') > 0)
        {
          $(this).addClass('has-background-dark');
          $(this).append(' ['+$(this).attr('dt-order')+']');

          if(update_character) character.push($(this).attr('dt-id'));
        }
      }
      else
      {
        if($(this).attr('dt-selected') > 0)
        {
          var filter_id = $(this).attr('dt-id');
          switch(selector) {
            case 'religion':
              religion = filter_id;
              break;
            case 'gender':
              gender = filter_id;
              break;
          }

          $(this).addClass('has-background-dark');
        }
      }
    });
  }

  refresh_filter(true);
});
