$(document).ready(function()
{
  var religion = [] , gender = [] , character = [];
  var total = -1;

  $(document)
  .on('mouseover', '.filter', function()
  {
    $(this).addClass('has-background-black-ter');
  })
  .on('mouseleave', '.filter', function()
  {
    $(this).removeClass('has-background-black-ter');
  })
  .on('click', '.filter', function()
  {
    var selector = $(this).closest('.filter-group').attr('dt-group');
    var selector_id = $(this).attr('dt-id');
    if($(this).hasClass('active'))
    {
      switch(selector) {
        case 'religion':
          religion.splice( religion.indexOf(selector_id) , 1 );
          break;

        case 'gender':
          gender.splice( gender.indexOf(selector_id) , 1 );
          break;

        case 'character':
          character.splice( character.indexOf(selector_id) , 1 );
          break;
      }

      $(this).removeClass('active');
    }
    else
    {
      switch(selector) {
        case 'religion':
          religion.push(selector_id);
          break;

        case 'gender':
          gender.push(selector_id);
          break;

        case 'character':
          character.push(selector_id);
          break;
      }

      $(this).addClass('active');
    }

    get_user(true);
  })
  .on('click', '.button-filter', function()
  {
    var icon = $(this).find('.icon i');
    if($(this).hasClass('active'))
    {
      $(this).removeClass('active');
      $('.filter-groups').slideUp(500 , function() {
        icon.attr('class' , 'fas fa-search');
      });
    }
    else
    {
      $(this).addClass('active');
      $('.filter-groups').slideDown(500 , function() {
        icon.attr('class' , 'far fa-times-circle');
      });
    }
  });

  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
      if(total != ($(".user .column").length - 1))get_user(false);
    }
  });

  function get_user(restart)
  {
    var data = {
      religion : JSON.stringify(religion),
      gender : JSON.stringify(gender),
      character : JSON.stringify(character),
      offset : restart ? 0 : $(".user .column").length - 1
    };

    $.ajax({
      type : 'POST',
      url : $('.columns.user').attr('dt-url'),
      data : data,
      beforeSend : function() {
        NProgress.configure({
          showSpinner: false
        }).start();
      },
      success: function (data) {
        if(restart)
        {
          $('.user .column').each(function( index )
          {
            if(!$(this).hasClass('hide')) $(this).remove();
          });
        }

        total = data.total;
        for(a = 0 ; a < data.user.length ; a++)
        {
          var user = data.user[a];

          var selector = $('.user .column.hide');
          selector.find('.image img').attr('src' , user.image);
          selector.find('.image img').attr('alt' , user.name);
          selector.find('.card-image .score').text(user.score);
          if(user.specs)
          {
            selector.find('.card-image .specs').removeClass('hide');
            selector.find('.card-image .specs').text(user.specs);
          }
          else
          {
            selector.find('.card-image .specs').addClass('hide');
          }

          selector.find('.media-content .title').text(user.name);
          selector.find('a').attr('href' , 'https://twitter.com/' + user.screen_name);
          selector.find('.media-content .subtitle span').text('@' + user.screen_name);
          selector.find('.content').text(user.description);
          var user_template = selector.html();

          $('.user').append('<div class="column is-half-tablet is-one-third-widescreen is-one-quarter-fullhd">' + user_template + '</div');
        }

        var image_id = 0;
        $('img').each(function()
        {
            $(this).on("error", function()
            {
                $(this).attr('src' , 'https://picsum.photos/480/?image=' + image_id);
                image_id++;
            });
        });

        NProgress.done();
      }
    });
  }
  get_user(true);
});
