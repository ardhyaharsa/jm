$(document).ready(function()
{
	$(document)
	.on('click', '.navbar-burger', function()
	{
		var target = $(this).attr('data-target');
		if($(this).hasClass('is-active'))
		{
			$(this).removeClass('is-active');
			$('#'+target).removeClass('is-active');
		}
		else
		{
			$(this).addClass('is-active');
			$('#'+target).addClass('is-active');
		}
	})
	.on('keyup', '.numeral', function()
	{
		var value = $(this).val();
		if(value.length > 0)
		{
			var number = numeral(value);
			$(this).val(number.format('0,0'));
		}
	});
});
