<?php
  use \Interop\Container\ContainerInterface as ContainerInterface;

  class Cron
  {
    protected $container;

    public function __construct(ContainerInterface $container)
    {
      $this->container = $container;
    }

    public function __invoke($request, $response, $args)
    {
      return $response;
    }

    public function user($request, $response, $args)
    {
      $where = [
        'is_active' => 1,
      ];
      $user = User_::where($where)->get();
      /*
      foreach($user as $k=>$v)
      {
        $null = [NULL];
        $height = array_merge($null , range(150,185));
        $weight = array_merge($null , range(50,100));
        $religion = array_merge($null , config('profile.religion'));
        $gender = array_merge($null , config('profile.gender'));
        $religion_selected = array_rand($religion);
        $gender_selected = array_rand($gender);

        $day = range(6205,18250);
        $time = new DateTime('now');
        $birthdate = $time->modify('-'.$day[array_rand($day)].' day')->format('Y-m-d');

        $data = [
          'height' => $height[array_rand($height)],
          'weight' => $weight[array_rand($weight)],
          'religion' => $religion_selected ? $religion_selected : NULL,
          'gender' => $gender_selected ? $gender_selected : NULL,
          'birthdate' => (bool) rand(0,1) ? $birthdate : NULL
        ];
        User_::where('id' , val($v , 'id'))->update($data);
      }
      */

      return $response->withJson($user , 404 , JSON_PRETTY_PRINT);
    }

    private function random_word($type , $length = 10)
    {
      switch($type)
      {
        case 'i':
          $characters = '0123456789';
          break;
          case 's':
            $characters = '0123456789 abcde fghij klmno pqrst uvwxyz ABCDE FGHIJ KLMNO PQRST UVWXYZ';
            break;
      }

      $charactersLength = strlen($characters);
      $random = '';
      for ($i = 0; $i < $length; $i++)
      {
          $random .= $characters[rand(0, $charactersLength - 1)];
      }

      return $type == '1' ? (int) $random : $random;
    }
  }
