<?php
  use \Interop\Container\ContainerInterface as ContainerInterface;
  use Abraham\TwitterOAuth\TwitterOAuth;

  class Profile
  {
    protected $container;

    public function __construct(ContainerInterface $container)
    {
      $this->container = $container;
    }

    public function __invoke($request, $response, $args)
    {
      return $response;
    }

    public function index($request , $response , $args)
    {
      if(val($_SESSION , 'id'))
      {
        $data = [
          'key' => 'profile',
          'title' => val($_SESSION , 'name'),
          'custom_css' => ['daterangepicker','content/profile'],
          'custom_js' => ['numeral.min','moment.min','daterangepicker.min','notify','content/profile'],
          'content' => $this->_build_content()
        ];

        return render($this->container , $response , $data);
      }
      else
      {
        return $response->withRedirect(config('url.site'));
      }
    }

    public function twitter($request , $response , $args)
    {
      $data = [];
      $oauth_token = $request->getParam('oauth_token');
      $oauth_verifier = $request->getParam('oauth_verifier');
      $denied = $request->getParam('denied');

      if($oauth_token || $oauth_verifier)
      {
        $connection = new TwitterOAuth(config('socials.twitter.consumer_key') , config('socials.twitter.consumer_secret'));

        $oauth = [
          'oauth_token' => $oauth_token,
          'oauth_verifier' => $oauth_verifier
        ];
        $access_token = $connection->oauth('oauth/access_token', $oauth);

        $connection = new TwitterOAuth(config('socials.twitter.consumer_key') , config('socials.twitter.consumer_secret') , $access_token['oauth_token'] , $access_token['oauth_token_secret']);
        $user_info = $connection->get('account/verify_credentials');

        $social = [
          'twitter' => [
            'user_info' => $user_info,
            'access_token' => $access_token
          ]
        ];

        $data = [
          'social_id' => val($user_info , 'id'),
          'name' => val($user_info , 'name'),
          'screen_name' => val($user_info , 'screen_name'),
          'description' => val($user_info , 'description'),
          'image' => json_encode([
            'banner' => val($user_info , 'profile_banner_url'),
            'profile' => val($user_info , 'profile_image_url_https')
          ]),
          'following' => val($user_info , 'friends_count'),
          'follower' => val($user_info , 'followers_count'),
          'social' => json_encode($social)
        ];

        $where = [
          'social_id' => val($user_info , 'id')
        ];
        $user = User_::where($where)->first();
        if($user)
        {
          User_::where($where)->update($data);
          $userID = val($user , 'id');
        }
        else
        {
          $data['is_active'] = FALSE;
          $data['create_date'] = date('Y-m-d H:i:s');

          $userID = User_::insertGetId($data);
        }

        $data = User_::where('id' , $userID)->first()->toArray();

        $time = new DateTime('now');
        if(is_null($data['birthdate'])) $data['birthdate'] = $time->modify('-17 year')->format('Y-m-d');
        $_SESSION = $data;

        $url = url('profile/');
      }
      else if($denied)
      {
        $url = url('');
      }
      else
      {
        $connection = new TwitterOAuth(config('socials.twitter.consumer_key') , config('socials.twitter.consumer_secret'));
        $oauth = [
          'oauth_callback' => config('socials.twitter.callback_url')
        ];
        $request_token = $connection->oauth('oauth/request_token' , $oauth);

        $oauth = [
          'oauth_token' => $request_token['oauth_token']
        ];

        $url = $connection->url('oauth/authorize' , $oauth);
      }

      return $response->withRedirect($url);
    }

    public function character($request , $response , $args)
    {
      $height = $request->getParam('height');
      $weight = $request->getParam('weight');
      $birthdate = new DateTime($request->getParam('birthdate'));
      $religion = $request->getParam('religion');
      $gender = $request->getParam('gender');
      $character = $request->getParam('character');

      $data = [
        'height' => !empty($height) ? $height : NULL,
        'weight' => !empty($weight) ? $weight : NULL,
        'birthdate' => $birthdate->format('Y-m-d'),
        'religion' => $religion ? $religion : NULL,
        'gender' => $gender ? $gender : NULL,
        'characteristic' => $character,
        'is_active' => TRUE
      ];
      User_::where('id' , val($_SESSION , 'id'))->update($data);

      $_SESSION = array_merge($_SESSION , $data);
      $result = [
        'result' => TRUE
      ];
      return $response->withJson($data);
    }

    public function delete($request , $response , $args)
    {
      $data = [
        'characteristic' => NULL,
        'is_active' => FALSE
      ];
      User_::where('id' , val($_SESSION , 'id'))->update($data);

      session_destroy();
      return $response->withRedirect(config('url.site'));
    }

    public function sign_out($request , $response , $args)
    {
      session_destroy();
      return $response->withRedirect(config('url.site'));
    }

    private function _build_content()
    {
      $image = json_decode(val($_SESSION , 'image'));
      $user_character = json_decode(val($_SESSION , 'characteristic' , '[]'));

      $character = Character_::get();
      foreach($character as $k=>$v)
      {
        $v['order'] = 0;
        if(in_array($v['id'] , $user_character))
        {
          $v['order'] = array_search($v['id'] , $user_character) + 1;
        }

        $character[$k] = $v;
      }

      $religion = [];
      foreach(config('profile.religion') as $k=>$v)
      {
        $religion[$k] = [
          'text' => $v,
          'selected' => $_SESSION['religion'] == $k ? 1 : 0,
        ];
      }

      $gender = [];
      foreach(config('profile.gender') as $k=>$v)
      {
        $gender[$k] = [
          'text' => $v,
          'selected' => $_SESSION['gender'] == $k ? 1 : 0,
        ];
      }

      $birthdate = new DateTime($_SESSION['birthdate']);
      $data = [
        'image' => [
          'banner' => val($image , 'banner') . '/1500x500',
          'profile' => str_replace('_normal' , '' , val($image , 'profile')) ,
        ],
        'height' => $_SESSION['height'],
        'weight' => $_SESSION['weight'],
        'birthdate' => $birthdate->format('d-m-Y'),
        'character' => $character,
        'religion' => $religion,
        'gender' => $gender,
        'active' => $_SESSION['is_active']
      ];

      return $this->container->view->fetch('content/_profile.twig' , $data);
    }
  }
