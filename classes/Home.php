<?php
  use \Interop\Container\ContainerInterface as ContainerInterface;

  class Home
  {
    protected $container;

    public function __construct(ContainerInterface $container)
    {
      $this->container = $container;
    }

    public function __invoke($request, $response, $args)
    {
      return $response;
    }

    public function index($request, $response, $args)
    {
      $data = [
        'key' => 'home',
        'custom_css' => ['content/home'],
        'custom_js' => ['content/home'],
        'content' => $this->_build_content()
      ];

      return render($this->container , $response , $data);
    }

    public function user($request , $response , $args)
    {
      $score = $user = [];
      $top_score = 0;

      $gender = json_decode($request->getParam('gender') , TRUE);
      $religion = json_decode($request->getParam('religion') , TRUE);
      $character = json_decode($request->getParam('character') , TRUE);
      foreach($character as $k=>$v)
      {
        $score[$v] = (count($character) - $k) * count($character);
      }
      $offset = $request->getParam('offset');
      $limit = 24;

      $where = [
        'is_active' => TRUE
      ];
      $user_temp = User_::select('id','name','screen_name','description','image','height','weight','birthdate','characteristic')->where($where);
      if(!empty($gender)) $user_temp = $user_temp->whereIn('gender' , $gender);
      if(!empty($religion)) $user_temp = $user_temp->whereIn('religion' , $religion);

      foreach($user_temp->get() as $k=>$v)
      {
        $image = json_decode(val($v , 'image'));
        $v['image'] = str_replace('_normal' , '' , val($image , 'profile'));

        $user_score = 0;
        $characteristic = is_null($v['characteristic']) ? '[]' : val($v , 'characteristic');
        $characteristic = json_decode($characteristic , TRUE);
        foreach($characteristic as $k2=>$v2)
        {
          $user_score += (val($score , $v2 , 0) * (count($characteristic) - $k2)) / count($characteristic);
        }
        $v['score'] = $user_score;
        if($user_score > $top_score) $top_score = $user_score;

        $add_info = [];
        if(!is_null($v['height'])) $add_info[] = round(val($v , 'height') / 100 , 1) . 'm';
        if(!is_null($v['weight'])) $add_info[] = val($v , 'weight') . 'kg';
        if(!is_null($v['birthdate']))
        {
          $start = new DateTime($v['birthdate']);
          $end  = new DateTime('now');
          $diff = $start->diff($end);
          $add_info[] = $diff->format('%yth');
        }
        $v['specs'] = count($add_info) > 0 ? implode(' | ' , $add_info) : FALSE;

        $unset = ['height','weight','birthdate','characteristic'];
        foreach($unset as $k2=>$v2)
        {
          unset($v[$v2]);
        }

        $user[] = $v;
      }

      $user_fix = [];
      foreach($user as $k=>$v)
      {
        $v['score'] = $top_score > 0 ? round(($v['score'] / $top_score) * 100) : 100;
        $user_fix[$v['score'].'_'.$v['id']] = $v;
      }
      uksort($user_fix , 'strnatcasecmp');
      $user_fix = array_values(array_reverse($user_fix));
      $total = count($user_fix);

      if(count($user_fix) < $offset) {
        $offset = 0;
      }
      else {
        $limit = $offset + $limit;
        if($limit > count($user_fix)) $limit = count($user_fix);
      }

      $result = [
        'user' => array_slice($user_fix , $offset , $limit),
        'total' => $total
      ];
      return $response->withJson($result);
    }

    private function _build_content()
    {
      $character = Character_::get();

      $data = [
        'character' => $character,
        'religion' => config('profile.religion'),
        'gender' => config('profile.gender'),
        'list' => $this->container->view->fetch('box/home/_list.twig' , [])
      ];

      return $this->container->view->fetch('content/_home.twig' , $data);
    }
  }
