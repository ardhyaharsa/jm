<?php

class User_ extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    public $timestamps = false;
    protected $hidden = ['social'];
}
