<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

session_start();
$app = new \Slim\App(config());
$container = $app->getContainer();

$container['view'] = function ($c)
{
  $view = new \Slim\Views\Twig('../templates/');

  $env = $view->getEnvironment();
  $env->addGlobal('url' , config('url'));
  $env->addGlobal('site_name' , config('site_name'));
  $env->addGlobal('version' , config('assets.version'));

  return $view;
};

$container['logger'] = function($c)
{
  $logger = new \Monolog\Logger('my_logger');
  $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
  $logger->pushHandler($file_handler);

  return $logger;
};

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$capsule->getContainer()->singleton
(
  Illuminate\Contracts\Debug\ExceptionHandler::class , App\Exceptions\Handler::class
);

$app->get('/', \Home::class . ':index');

$app->group('/home', function () use ($container)
{
  $this->post('/user', \Home::class . ':user');
});

$app->group('/profile', function () use ($container)
{
  $this->get('/', \Profile::class . ':index');
  $this->get('/twitter', \Profile::class . ':twitter');
  $this->post('/character', \Profile::class . ':character');
  $this->get('/delete', \Profile::class . ':delete');
  $this->get('/sign_out', \Profile::class . ':sign_out');
});

$app->group('/cron', function () use ($container)
{
  $this->get('/user', \Cron::class . ':user');
});

$app->run();
