<?php
  function config($cnf = '')
  {
    $url = 'http://172.20.10.2/jm/';

    $data = [
      'site_name' => '#jodohmalang',
      'settings' => [
        'displayErrorDetails' => TRUE,
        'determineRouteBeforeAppMiddleware' => FALSE,
        'db' => [
          'driver' => 'mysql',
          'host' => 'localhost',
          'database' => 'jm',
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8mb4',
          'collation' => 'utf8mb4_bin',
          'prefix' => '',
        ]
      ],
      'url' => [
        'site' => $url.'public/',
        'asset' => $url
      ],
      'assets' => [
        'version' => '1.2',
        'directory' => [
          'css' => '../resources/css/',
          'js' => '../resources/js/',
        ],
        'url' => [
          'css' => 'resources/css/',
          'js' => 'resources/js/',
        ],
      ],
      'profile' => [
        'religion' => [
          'islam' => 'Islam',
          'christian' => 'Kristen',
          'catholic' => 'Katolik',
          'hindu' => 'Hindu',
          'budha' => 'Budha',
          'konghuchu' => 'Kong Hu Cu'
        ],
        'gender' => [
          'men' => 'Pria',
          'women' => 'Wanita'
        ]
      ],
      'socials' => [
        'twitter' => [
          'consumer_key' => '28Z5hBfUEGyK1eBDPkbHYOvj8',
          'consumer_secret' => 'BZhm9h2VKul6qeEmqUeULMVtDMFS9XNAUw9zWiUN0aQ6irpwMM',
          'callback_url' => $url . 'public/profile/twitter',
        ]
      ]
    ];

    return ($cnf == '') ? $data : val($data , $cnf);
  }

  function url($uri = '')
  {
    return config('url.site') . $uri;
  }

  function echoPre($data)
  {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
  }

  function toArray($data)
  {
    return json_decode(json_encode($data), true);
  }

  function val($row , $key , $default = '')
  {
    if ( is_object($row) ) $row = toArray($row);

    //sub key
    if ( strpos($key, '.') )
    {
      $arr = explode('.', $key);
      for($i = 0 ; $i < count($arr) - 1 ; $i++)
      {
        $row = val($row , $arr[$i] , $default);
      }

      return val($row , end($arr) , $default);
    }
    else
    {
      return isset($row[$key]) && $row[$key] ? $row[$key] : $default;
    }
  }

  function createDirectory($directory = '' , $permission = 0777)
  {
    if (!file_exists($directory)) mkdir($directory , $permission , TRUE);
  }
