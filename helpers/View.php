<?php
  use MatthiasMullie\Minify;

  function render($container , $response , $data)
  {
    $title = val($data , 'title') ? val($data , 'title').' | '.config('site_name') : config('site_name');
    $description = val($data , 'description') ? val($data , 'description') : 'Biro Jodoh dari warga Malang , untuk warga Malang';
    $key = val($data , 'key' , 'home');

    $custom_js = ['jquery-3.3.1.min','nprogress','global'];
    $custom_js = array_merge($custom_js , val($data , 'custom_js' , []));
    $js = build_js($key , $custom_js);

    $custom_css = ['bulma','fontawesome-all.min','nprogress','global'];
    $custom_css = array_merge($custom_css , val($data , 'custom_css' , []));
    $css = build_css($key , $custom_css);

    $data = [
      'title' => $title,
      'description' => $description,
      'custom_css' => $css,
      'header' => build_header($container),
      'content' => val($data , 'content' , ''),
      'footer' => build_footer($container),
      'custom_js' => $js,
    ];

    return $container->view->render($response , 'url/_main.twig' , $data);
  }

  function build_js($name , $data)
  {
    $minifier = new Minify\JS();
    $directory = config('assets.directory.js');

    foreach($data as $k=>$v)
    {
      $minifier->add($directory . $v.'.js');
    }

    $generate_dir = $directory . 'generate/' . config('assets.version') . '/';
    createDirectory($generate_dir);
    $minifiedPath = $generate_dir . $name . '.js';
    $minifier->minify($minifiedPath);

    $minifiedURL = config('url.asset') . str_replace($directory , config('assets.url.js') , $minifiedPath);
    return '<script type="text/javascript" src="' . $minifiedURL . '"></script>';
  }

  function build_css($name , $data)
  {
    $minifier = new Minify\CSS();
    $directory = config('assets.directory.css');

    foreach($data as $k=>$v)
    {
      $minifier->add($directory . $v.'.css');
    }

    $generate_dir = $directory . 'generate/' . config('assets.version') . '/';
    createDirectory($generate_dir);
    $minifiedPath = $generate_dir . $name . '.css';
    $minifier->minify($minifiedPath);

    $minifiedURL = config('url.asset') . str_replace($directory , config('assets.url.css') , $minifiedPath);
    return "<link rel=\"stylesheet\" href=\"" . $minifiedURL ."\" defer=\"defer\" />";
  }

  function build_header($container)
  {
    $login_url = url('profile/twitter');
    $params = [
      'login_url' => $login_url,
      'name' => val($_SESSION , 'name'),
      'image' => json_decode(val($_SESSION , 'image') , TRUE),
    ];

    $template = val($_SESSION , 'id') ? '' : '_no';
    $data = [
      'navbar_start' => $container->view->fetch('box/global/header/navbar/_start.twig' , $params),
      'navbar_end' => $container->view->fetch('box/global/header/navbar/_end_login'.$template.'.twig' , $params),
      'login_url' => $login_url,
      'is_login' => val($_SESSION , 'id')
    ];

    return $container->view->fetch('box/global/_header.twig' , $data);
  }

  function build_footer($container)
  {
    $data = [];

    return $container->view->fetch('box/global/_footer.twig' , $data);
  }
